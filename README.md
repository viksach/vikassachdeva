# Parental Control Service

Implementation of ParentalControlService API .

This project is created using:
1. Java 8
2. junit:4.12
3. mockito-core:2.10.0
4. gradle

## ParentalControlService API:
This Parental Control Service requires reference of movie service and ordered list of rating levels. It provides :

boolean isAllowedToWatch(Rating customerLevel, String movieId, ErrorMessage message)

Returns true if the customer is allowed to watch the movie or else false.
ErrorMessage object is used to update the error message to the calling client if any error occurs.

Parental Control Service class is implemented at 
src/main/java/sky/parentalcontrol/service/ParentalControlService.java

## Test Implementation:
Unit Test class is 
src/test/java/sky/parentalcontrol/service/ParentalControlServiceTest.java

## Running the Test
./gradlew clean test

Test report is available at:
build/reports/tests/index.html

