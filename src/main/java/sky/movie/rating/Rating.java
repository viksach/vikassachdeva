package sky.movie.rating;


public enum Rating {
    U("U"),
    PG("PG"),
    R12("R12"),
    R15("R15"),
    R18("R18");

    private final String code;

    Rating(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
