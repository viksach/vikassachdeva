package sky.movie.exception;

public class TitleNotFoundException extends Exception {

    private static final long serialVersionUID = -5457911173757009301L;

    public TitleNotFoundException(String message) {
        super(message);
    }
}
