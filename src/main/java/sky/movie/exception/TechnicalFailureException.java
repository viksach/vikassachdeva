package sky.movie.exception;

public class TechnicalFailureException extends Exception {
    private static final long serialVersionUID = 5984837578506280480L;

    public TechnicalFailureException(String message) {
        super(message);
    }
}
