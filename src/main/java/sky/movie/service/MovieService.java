package sky.movie.service;

import sky.movie.exception.TechnicalFailureException;
import sky.movie.exception.TitleNotFoundException;
import sky.movie.rating.Rating;

public interface MovieService {
    Rating getParentalControlLevel(String movieId) throws TitleNotFoundException, TechnicalFailureException;
}
