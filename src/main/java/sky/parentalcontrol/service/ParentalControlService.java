package sky.parentalcontrol.service;

import sky.movie.exception.TechnicalFailureException;
import sky.movie.exception.TitleNotFoundException;
import sky.movie.rating.Rating;
import sky.movie.service.MovieService;
import sky.parentalcontrol.error.ErrorMessage;

import java.util.Comparator;
import java.util.List;

public class ParentalControlService {

    private final MovieService movieService;
    private final List<Rating> ordinalRatingLevels;

    public ParentalControlService(MovieService movieService, List<Rating> ordinalRatingLevels) {
        this.movieService = movieService;
        this.ordinalRatingLevels = ordinalRatingLevels;
    }

    public boolean isAllowedToWatch(Rating customerLevel, String movieId, ErrorMessage message) {
        try {
            final Rating movieLevel = movieService.getParentalControlLevel(movieId);

            return isCustomerLevelHigherThanMovieLevel(customerLevel, movieLevel);
        } catch (TitleNotFoundException | TechnicalFailureException e) {
            message.setMessage(e.getMessage());
            return false;
        }
    }

    private boolean isCustomerLevelHigherThanMovieLevel(Rating customerLevel, Rating movieLevel) {
        Comparator<Rating> levelComparator = Comparator.comparingInt(ordinalRatingLevels::indexOf);
        return levelComparator.compare(customerLevel, movieLevel) >= 0;
    }
}
