package sky.parentalcontrol.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import sky.movie.exception.TechnicalFailureException;
import sky.movie.exception.TitleNotFoundException;
import sky.movie.rating.Rating;
import sky.movie.service.MovieService;
import sky.parentalcontrol.error.ErrorMessage;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static sky.movie.rating.Rating.*;

@RunWith(MockitoJUnitRunner.class)
public class ParentalControlServiceTest {

    private static final String SOME_MOVIE_ID = "SomeMovieId";

    private static final String ERR_MSG_NONE = "None";
    private static final String ERR_MSG_TITLE_NOT_FOUND = "Title Not Found";
    private static final String ERR_MSG_TECHNICAL_FAILURE = "Technical Failure";

    @InjectMocks
    private ParentalControlService parentalControlService;

    @Mock
    private MovieService movieService;

    private ErrorMessage errorMessage;

    private List<Rating> ordinalRatingLevels = Arrays.asList(U, PG, R12, R15, R18);

    @Before
    public void setup() throws Exception {
        initMocks(this);
        parentalControlService = new ParentalControlService(movieService, ordinalRatingLevels);
        errorMessage = new ErrorMessage(ERR_MSG_NONE);
    }

    @Test
    public void givenCustomerLevelIsHigherThanMovieLevelUserIsAllowedToWatch() throws Exception{
        when(movieService.getParentalControlLevel(SOME_MOVIE_ID)).thenReturn(R12);
        assertTrue(parentalControlService.isAllowedToWatch(R18, SOME_MOVIE_ID, errorMessage));
        verify(movieService, times(1)).getParentalControlLevel(SOME_MOVIE_ID);
        assertEquals(ERR_MSG_NONE, errorMessage.getMessage());
    }

    @Test
    public void givenCustomerLevelIsSameAsMovieLevelUserIsAllowedToWatch() throws Exception{
        when(movieService.getParentalControlLevel(SOME_MOVIE_ID)).thenReturn(R15);
        assertTrue(parentalControlService.isAllowedToWatch(R15, SOME_MOVIE_ID, errorMessage));
        verify(movieService, times(1)).getParentalControlLevel(SOME_MOVIE_ID);
        assertEquals(ERR_MSG_NONE, errorMessage.getMessage());
    }

    @Test
    public void givenCustomerLevelIsLowerThanMovieLevelUserIsNOTAllowedToWatch() throws Exception{
        when(movieService.getParentalControlLevel(SOME_MOVIE_ID)).thenReturn(R12);
        assertFalse(parentalControlService.isAllowedToWatch(PG, SOME_MOVIE_ID, errorMessage));
        verify(movieService, times(1)).getParentalControlLevel(SOME_MOVIE_ID);
        assertEquals(ERR_MSG_NONE, errorMessage.getMessage());
    }

    @Test
    public void givenTitleNotFoundByMovieServiceShowErrorMessage() throws Exception{
        when(movieService.getParentalControlLevel(SOME_MOVIE_ID)).thenThrow(new TitleNotFoundException(ERR_MSG_TITLE_NOT_FOUND));
        assertFalse(parentalControlService.isAllowedToWatch(PG, SOME_MOVIE_ID, errorMessage));
        verify(movieService, times(1)).getParentalControlLevel(SOME_MOVIE_ID);
        assertEquals(ERR_MSG_TITLE_NOT_FOUND, errorMessage.getMessage());
    }

    @Test
    public void givenTechnicalFailureInMovieServiceShowErrorMessage() throws Exception{
        when(movieService.getParentalControlLevel(SOME_MOVIE_ID)).thenThrow(new TechnicalFailureException(ERR_MSG_TECHNICAL_FAILURE));
        assertFalse(parentalControlService.isAllowedToWatch(PG, SOME_MOVIE_ID, errorMessage));
        verify(movieService, times(1)).getParentalControlLevel(SOME_MOVIE_ID);
        assertEquals(ERR_MSG_TECHNICAL_FAILURE, errorMessage.getMessage());
    }

}
